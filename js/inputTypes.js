const type = Object.freeze({
    number: "number",
    operator: "operator",
    calculate: "calculate",
    clear: "clear",
    error: "error"
});

const typeOfInput = function (input) {
    switch (true) {
        case /[0-9\.]/.test(input):
            return type.number;
        case /[\-+x÷]/.test(input):
            return type.operator;
        case input === '.':
            return type.dot;
        case input === '=':
            return type.calculate;
        case input === "CE" || input === "AC":
            return type.clear;
        default:
            return;
    }
}

export { type, typeOfInput }