import $ from 'jquery';

import '../style/main.scss';
import { type, typeOfInput } from './inputTypes';
import { Expression } from './expression';
import { calculate } from './calculate';
import { setPrecision, formatNumber, validNumber } from './helper';

$(document).ready(function() {  
  // User interfaces
  const 
    $inputScreen   = $("#expression"),
    $outputScreen = $("#result"),
    $keys = $(".key");
  
  const inputHandler = (function() {    
    const expr = new Expression();
    
    let 
      curType       = null,
      prevType     = null;
    
    function getInput() {
      return $inputScreen.text();
    }
    
    function getOutput() {
      return $outputScreen.text();
    }
    
    function writeInput(input) {
      $inputScreen.text(input);
    }
    
    function writeOutput(input) {
      $outputScreen.text(input);
    }

    // Assign handler function to each type
    const handler = {};
    // NUMBER
    handler[type.number] = function(input) {
      if (prevType === type.calculate || prevType === type.error)
        handler[type.clear]();
      
      // Get operand determines which operand to use.
      const prevNum = expr.getOperand();
      if ( validNumber( prevNum + input) ) {
        expr.setOperand( formatNumber(prevNum, input) );
        writeInput(expr.getExpression());
        prevType = type.number;
      }
    }
    // OPERATOR
    handler[type.operator] = function(input) {
      // If followed by calculate or clear, use output as operand.
      if ( prevType === type.clear || prevType === type.calculate ) {
        handler[type.number]( getOutput() );
      }
      // When expression is complete and operator is pressed
      // It should use its output as operand x and properly chain operator
      else if ( expr.complete() && prevType === type.number ) {
        handler[type.calculate]();
        handler[type.number]( getOutput() );
      }
      
      // Let operator be changed anytime.
      expr.setOperator( input );
      writeInput( expr.getExpression() );
      prevType = type.operator;
    }
    // CLEAR
    handler[type.clear] = function( input ) {
      if ( input === "AC" )
        writeOutput( '0' );
      
      expr.discard();
      writeInput( expr.getExpression() );
      prevType = type.clear;
    };
    // CALCULATE
    handler[type.calculate] = function( _ ) {
      if ( expr.complete() ) {
        // If follwed by chained calculate request, swap operand x from previous output.
        if ( prevType === type.calculate ) {
          expr.replace("x", getOutput() );
          writeInput( expr.getExpression() );
        }
        try {
          const result = calculate( expr )
          writeOutput( setPrecision(result, 7) );
          prevType = type.calculate;
        } catch (e) {
          handler[type.error](e);
        }
      }
    };
    // ERROR
    handler[type.error] = function(input) {
      writeInput( input );
      
      prevType = type.error;
    };
    
    // Actual inputHandler()
    return function(input) {
      curType = typeOfInput(input);
      // Invoke initial handler
      handler[curType]( input );
    };
  }());
  
  // Input interfaces
  $keys.click(function() {
    const input = $(this).text();
    
    inputHandler(input);
  });
  
  // Naive implementation of keyboard input
  const keyMap = {
    106 : 'x',
    88 : 'x',
    107 : '+',
    109 : '-',
    111 : '÷',
    32  : "CE",
    13 :  '=',
    187: '=',
    190: '.',
    110: '.'
  }
  keyMap.assignNumbers = function() {
    for (var i = 96; i <= 105; i++) {
      keyMap[i] = i % 48;
      keyMap[i - 48] = i % 48;
    }
  }();
  
  $(document).keydown(function(e) {
    // Disables default backspace behavior.
    if (e.which == 8)
      e.preventDefault();
    
    const knownKey = keyMap[e.keyCode];
    
    if (knownKey)
      inputHandler( knownKey )
  });

});