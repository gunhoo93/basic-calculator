// Assume internal function only exprects Expression object as an argument.
const methods = {
    '+': function (x, y) {
        return x + y;
    },
    '-': function (x, y) {
        return x - y;
    },
    'x': function (x, y) {
        return x * y;
    },
    '÷': function (x, y) {
        if (y === 0) throw new TypeError("no 0 division");
        return x / y;
    }
};

const calculate = function (expression) {
    const {x, y, operator} = expression;

    return methods[operator](Number(x), Number(y));
};

export { calculate };