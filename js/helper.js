// Naive version to control precision.
const setPrecision = (n, precision) => {
    precision = precision || 7;
    n = Number(n);
    var sigDigit = Math.pow(10, precision);

    return Math.round(n * sigDigit) / sigDigit;
};

// Checks for number string and return false if it has more than 1 dots
const validNumber = (numstr) => {
    var dots = 0;

    for (var i = 0; i < numstr.length; i++) {
        if (numstr[i] === '.') dots += 1;
        if (dots > 1) return false;
    }
    return true;
};

const formatNumber = (prevNum, input) => {
    switch (true) {
        case prevNum === "" && input === '.':
            return '0' + input;
        case prevNum.startsWith('0.'):
            return prevNum + input;
        case prevNum.startsWith('0') && /[0-9]/.test(input):
            return input;
        default:
            return prevNum + input;
    }
};

export { setPrecision, validNumber, formatNumber };