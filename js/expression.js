function Expression(x, y, operator) {
    this.x = x || '';
    this.y = y || '';
    this.operator = operator || '';
}
Expression.prototype = {
    getOperand: function () {
        if (this.operator)
            return this.y;
        return this.x;
    },

    setOperand: function (numstr) {
        if (this.operator)
            this.y = numstr;
        else
            this.x = numstr;
        return this;
    },

    setOperator: function (oprstr) {
        this.operator = oprstr;
        return this;
    },

    getExpression: function () {
        return this.x + this.operator + this.y;
    },

    complete: function () {
        return !!(this.x && this.y && this.operator);
    },

    discard: function () {
        this.x = '';
        this.y = '';
        this.operator = '';
    },

    replace: function (key, val) {
        if (this.hasOwnProperty(key)) {
            this[key] = val;
        } else {
            throw new Error(`Internal Error: expr.replace() failed, ${key} not found`);
        }
        return this;
    }
}

export { Expression }